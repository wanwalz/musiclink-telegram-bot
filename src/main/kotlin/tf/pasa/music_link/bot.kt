package tf.pasa.music_link

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.elbekD.bot.Bot
import com.elbekD.bot.types.InlineKeyboardButton
import com.elbekD.bot.types.InlineKeyboardMarkup
import com.elbekD.bot.types.Message
import redis.clients.jedis.Jedis
import redis.clients.jedis.exceptions.JedisConnectionException
import java.net.URI
import java.net.URLEncoder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse


val URL_REGEX = Regex("(https?://)?([-a-zA-Z0-9+&@#%?=~_|!:,.;]+)/[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
val PLATFORMS = listOf("spotify", "appleMusic", "youtube", "deezer", "soundcloud")
val PLATFORMS_NAMES = mapOf(
    "spotify" to "Spotify",
    "appleMusic" to "Apple Music",
    "youtube" to "Youtube",
    "deezer" to "Deezer",
    "soundcloud" to "Soundcloud"
)

val jedis = try {
    val instance = Jedis("redis")
    instance.get("foo")
    instance
} catch (e: JedisConnectionException) {
    null
}


fun main() {
    val token = System.getenv("TELEGRAM_TOKEN")!!
    val bot = Bot.createPolling("MusicLink",token)

    bot.onCommand("/start") { message: Message, _ ->
        val text = "I can help you share music with your friends, checkout /help"
        bot.sendMessage(message.chat.id,text)
    }

    bot.onCommand("/help") { message: Message, _ ->
        val text =
            "You can either: \n" +
            "\t- Send links directly to me and transfer my answers to your friends\n" +
            "\t- Add me to your groups and use /link <link to your song>\n\n" +
            "If you want an example, send me this link back: https://youtu.be/dQw4w9WgXcQ"
        bot.sendMessage(message.chat.id,text, preview = false)
    }

    bot.onMessage { message: Message ->
        URL_REGEX.findAll(message.text ?: "").forEach { match ->
            val url = match.value
            queryAPi(url)?.let { parseResponse(it, url) }?.let{
                answer(bot, message, it)
            }

        }
    }
    bot.onCommand("/link") { message: Message, url: String? ->
        url?.let {
            queryAPi(url)?.let { parseResponse(it, url) }?.let{
                answer(bot, message, it)
            }
        }
    }

    println("Bot starting..")
    bot.start()
}

fun answer(bot: Bot, message: Message, data: Triple<String, InlineKeyboardMarkup, String?>) {
    if (data.third == null) {
        bot.sendMessage(message.chat.id, data.first, markup = data.second)
    } else {
        bot.sendPhoto(message.chat.id, data.third!!, caption = data.first, markup = data.second)
    }
}

fun parseResponse(response: String, url: String): Triple<String, InlineKeyboardMarkup, String?>? {
    val json = Parser.default().parse(StringBuilder(response)) as JsonObject
    val inlineKeyboard = PLATFORMS.mapNotNull { platform ->
        val link = json.obj("linksByPlatform")?.obj(platform)?.string("url")
        link?.let { InlineKeyboardButton(PLATFORMS_NAMES.getOrDefault(platform, ""), link) }
    }

    val entitiesByUniqueId = json.obj("entitiesByUniqueId")
    val key = entitiesByUniqueId?.keys?.firstOrNull { it.startsWith("SPOTIFY_SONG") }
        ?: entitiesByUniqueId?.keys?.first() ?: ""
    val info = entitiesByUniqueId?.obj(key)

    val title = info?.let {
        "${it.string("title")} by ${it.string("artistName")}"
    } ?: "Sharable links"
    val thumbnailUrl = info?.string("thumbnailUrl")
    
    val sharableLink = InlineKeyboardButton("Shareable Link", "https://album.link/${url}")
    
    return if (inlineKeyboard.isNotEmpty()) {
        Triple(title, InlineKeyboardMarkup(listOf(inlineKeyboard, listOf(sharableLink))), thumbnailUrl)
    } else {
        null
    }
}

fun queryAPi(url: String): String? {
    println("Querying $url")

    val cache = jedis?.get(url)
    if (cache != null) {
        println("Cached")
        return cache
    }

    println("Ask api.song.link")
    val client = HttpClient.newBuilder().build()
    val request = HttpRequest.newBuilder()
        .uri(URI.create(
            "https://api.song.link/v1-alpha.1/links?url=${URLEncoder.encode(url, "utf-8")}&userCountry=FR"
        )).build();
    val response = client.send(request, HttpResponse.BodyHandlers.ofString())
    return if (response.statusCode() == 200) {
        val body = response.body()
        jedis?.set(url, body)
        body
    } else {
        null
    }
}
