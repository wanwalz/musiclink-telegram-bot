FROM gradle:6.3.0-jdk11 AS builder

WORKDIR /usr/src/musiclink
COPY . .
RUN ./gradlew build


FROM openjdk:11-jre-slim

WORKDIR /usr/src/factr
COPY --from=builder /usr/src/musiclink/build/distributions/music_link.tar .
RUN tar -xvf music_link.tar

COPY ./docker-entrypoint.sh .
ENTRYPOINT ["./docker-entrypoint.sh"]